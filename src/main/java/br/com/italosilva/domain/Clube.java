package br.com.italosilva.domain;

public class Clube {

    private String nome;

    private int pontos;

    public Clube(String nome, int pontos) {
        this.nome = nome;
        this.pontos = pontos;
    }

    public String getNome() {
        return nome;
    }

    public int getPontos() {
        return pontos;
    }

    @Override
    public String toString() {
        return new StringBuilder("Clube : ")
                .append(nome)
                .append(" Pontos: ")
                .append(pontos)
                .toString();
    }
}
