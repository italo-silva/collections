package br.com.italosilva.collections.mapas.treemap;

import br.com.italosilva.domain.Clube;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        /*Não possui chaves duplicadas, sua recorrencia substitui o valor,
         *Garante ordenacao conforme critério pré definid*/
        Clube corinthians = new Clube("Corinthians", 7);
        Clube audaxOsasco = new Clube("Audax Osasco", 7);
        Clube botafogo = new Clube("Botafogo", 0);
        Clube oesteBarueri = new Clube("Oeste Barueri", 7);

        Map<String, Clube> treeMap = new TreeMap<>();
        treeMap.put(corinthians.getNome(), corinthians);
        treeMap.put(audaxOsasco.getNome(), audaxOsasco);
        treeMap.put(botafogo.getNome(), botafogo);
        treeMap.put(oesteBarueri.getNome(), oesteBarueri);

        Clube clube = treeMap.get("Corinthians");
        System.out.println(clube);

        System.out.println("-------------> Percorrendo com for por chave");
        for (String chave : treeMap.keySet()){
            System.out.println(chave);
        }

        System.out.println("-------------> Percorrendo com for por valores");
        Collection<Clube> clubes = treeMap.values();
        for(Clube c : clubes) {
            System.out.println(c);
        }

        System.out.println("-------------> Percorrendo com for pelo entry set");
        Set<Map.Entry<String, Clube>> entries = treeMap.entrySet();
        for(Map.Entry<String, Clube> entry : entries) {
            System.out.println("Key : "+entry.getKey() +" Value: "+entry.getValue());
        }

    }
}
