package br.com.italosilva.collections.mapas.linkedhashmap;

import br.com.italosilva.domain.Clube;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        /*Não possui chaves duplicadas, sua recorrencia substitui o valor,
         *Garante ordenacao conforme ordem de insercao*/
        Clube corinthians = new Clube("Corinthians", 7);
        Clube audaxOsasco = new Clube("Audax Osasco", 7);
        Clube botafogo = new Clube("Botafogo", 0);
        Clube oesteBarueri = new Clube("Oeste Barueri", 7);

        Map<String, Clube> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(corinthians.getNome(), corinthians);
        linkedHashMap.put(audaxOsasco.getNome(), audaxOsasco);
        linkedHashMap.put(botafogo.getNome(), botafogo);
        linkedHashMap.put(oesteBarueri.getNome(), oesteBarueri);

        Clube clube = linkedHashMap.get("Corinthians");
        System.out.println(clube);

        System.out.println("-------------> Percorrendo com for por chave");
        for (String chave : linkedHashMap.keySet()){
            System.out.println(chave);
        }

        System.out.println("-------------> Percorrendo com for por valores");
        Collection<Clube> clubes = linkedHashMap.values();
        for(Clube c : clubes) {
            System.out.println(c);
        }

        System.out.println("-------------> Percorrendo com for pelo entry set");
        Set<Map.Entry<String, Clube>> entries = linkedHashMap.entrySet();
        for(Map.Entry<String, Clube> entry : entries) {
            System.out.println("Key : "+entry.getKey() +" Value: "+entry.getValue());
        }

    }
}
