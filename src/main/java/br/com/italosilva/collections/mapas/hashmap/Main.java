package br.com.italosilva.collections.mapas.hashmap;

import br.com.italosilva.domain.Clube;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        /*Não possui chaves duplicadas, sua recorrencia substitui o valor,
        * Não garante ordenacao conforme insercao*/
        Clube corinthians = new Clube("Corinthians", 7);
        Clube audaxOsasco = new Clube("Audax Osasco", 7);
        Clube botafogo = new Clube("Botafogo", 0);
        Clube oesteBarueri = new Clube("Oeste Barueri", 7);

        Map<String, Clube> hashMap = new HashMap<>();
        hashMap.put(corinthians.getNome(), corinthians);
        hashMap.put(audaxOsasco.getNome(), audaxOsasco);
        hashMap.put(botafogo.getNome(), botafogo);
        hashMap.put(oesteBarueri.getNome(), oesteBarueri);

        Clube clube = hashMap.get("Corinthians");
        System.out.println(clube);

        System.out.println("-------------> Percorrendo com for por chave");
        for (String chave : hashMap.keySet()){
            System.out.println(chave);
        }

        System.out.println("-------------> Percorrendo com for por valores");
        Collection<Clube> clubes = hashMap.values();
        for(Clube c : clubes) {
            System.out.println(c);
        }

        System.out.println("-------------> Percorrendo com for pelo entry set");
        Set<Map.Entry<String, Clube>> entries = hashMap.entrySet();
        for(Map.Entry<String, Clube> entry : entries) {
            System.out.println("Key : "+entry.getKey() +" Value: "+entry.getValue());
        }
    }
}
