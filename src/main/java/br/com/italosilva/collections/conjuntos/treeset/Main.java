package br.com.italosilva.collections.conjuntos.treeset;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {

        /*Possui ordenacao especifica (ordem de acordo com critérios definidos),
        não tem indice,
        não permite objetos iguais*/
        Set<String> treeSet = new TreeSet<>();

        treeSet.add("D4");
        treeSet.add("A7");
        treeSet.add("C5");
        treeSet.add("E3");
        treeSet.add("F2");
        treeSet.add("G1");
        treeSet.add("B6");
        treeSet.add("G1");

        for(String s : treeSet){
            System.out.println(s);
        }
    }
}
