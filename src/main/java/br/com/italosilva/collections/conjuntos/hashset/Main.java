package br.com.italosilva.collections.conjuntos.hashset;

import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        /*Não garante ordenacao por insercao,
        não tem indice,
        não permite objetos iguais*/
        Set<String> hashSet = new HashSet<>();

        hashSet.add("A7");
        hashSet.add("B6");
        hashSet.add("C5");
        hashSet.add("D4");
        hashSet.add("E3");
        hashSet.add("F2");
        hashSet.add("G1");
        hashSet.add("G1");

        for(String s : hashSet){
            System.out.println(s);
        }
    }
}
