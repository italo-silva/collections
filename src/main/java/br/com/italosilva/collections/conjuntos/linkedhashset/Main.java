package br.com.italosilva.collections.conjuntos.linkedhashset;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        /*Possui ordenacao especifica (ordem de insercao),
        não tem indice,
        não permite objetos iguais*/
        Set<String> linkedHashSet = new LinkedHashSet<>();

        linkedHashSet.add("D4");
        linkedHashSet.add("A7");
        linkedHashSet.add("C5");
        linkedHashSet.add("E3");
        linkedHashSet.add("F2");
        linkedHashSet.add("G1");
        linkedHashSet.add("B6");
        linkedHashSet.add("G1");

        for(String s : linkedHashSet){
            System.out.println(s);
        }
    }
}
