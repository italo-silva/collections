package br.com.italosilva.collections.list.arraylist;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        /*É armazenado em forma de array*/
        /*Listas aceitam valores duplicados*/
        List<String> arrayList = new ArrayList<>();

        arrayList.add("abc");
        arrayList.add("def");
        arrayList.add("zzz");
        arrayList.set(2, "ghi");
        arrayList.add(0, "Inicio");

        System.out.println("Percorrendo o array list com ehanced for");
        for (String texto : arrayList) {
            System.out.println(texto);
        }

        System.out.println("Percorrendo o array list com for");
        for (int a = 0; a < arrayList.size(); a++){
            System.out.println(arrayList.get(a));
        }

        System.out.println("Percorrendo o array list com o Iterator");
        Iterator iterator = arrayList.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
