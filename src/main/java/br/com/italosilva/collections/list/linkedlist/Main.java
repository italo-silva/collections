package br.com.italosilva.collections.list.linkedlist;

import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        /*É armazenado em forma de lista ligada*/
        /*Listas aceitam valores duplicados*/
        List<String> linkedList = new LinkedList<>();

        linkedList.add("def");
        linkedList.add("zzz");
        linkedList.add("abc");
        linkedList.set(2, "ghi");
        linkedList.add(0, "Inicio");

        System.out.println("Percorrendo o linked list");
        for (String a : linkedList) {
            System.out.println(a);
        }
    }
}
